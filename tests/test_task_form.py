import pytest
import uuid
import datetime

from twweb.forms import TaskForm

from helpers.utils import Input


@pytest.mark.parametrize('test_input', [
    Input(False, uuid='asdf'),
    Input(True, uuid=str(uuid.uuid4()))
])
def test_uuid(app, client, test_input):
    @app.route('/', methods=['POST'])
    def index():
        form = TaskForm()
        assert form.validate() is test_input.result
        return ''

    client.post('/', data=dict(test_input.data))
